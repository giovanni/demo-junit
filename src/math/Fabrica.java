/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package math;

import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author giovanni
 */
public class Fabrica {
   public static Operations createOperations(){
       ServiceLoader<Operations> loader = ServiceLoader.load(Operations.class);
       if(loader.iterator().hasNext()){
           return loader.iterator().next();
       }else {
           return null;
       }
   } 
}

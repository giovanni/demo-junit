/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package math;

/**
 *
 * @author giovanni
 */
public class MathMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Operations operations = Fabrica.createOperations();
        System.out.println("Sequencia de Fibonacci: ");
        for(int sequencia : operations.fibonacci(5)){
            System.out.print(sequencia + " ");
        }
    }
}

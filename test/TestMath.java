/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import math.Fabrica;
import math.Operations;

/**
 *
 * @author giovanni
 */
public class TestMath {
    
    public TestMath() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    @Test
    public void fibonacciTest(){
        Operations operations = Fabrica.createOperations();
        int maximo = 6;
        int[] sequencia = operations.fibonacci(maximo);
        int[] expecteds = new int[6];
        expecteds[0] = 1;
        expecteds[1] = 1;
        expecteds[2] = 2;
        expecteds[3] = 3;
        expecteds[4] = 5;
        expecteds[5] = 8;
        Assert.assertArrayEquals(expecteds, sequencia);
    }
}